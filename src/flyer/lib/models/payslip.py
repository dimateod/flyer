from collections import namedtuple

Payslip = namedtuple("Payslip", "pid payday data created_at")
