import datetime
import re

import openpyxl as xlsx

from flyer.lib.models import ProfileData
from flyer.lib.constants import PAYDAY

_MONTHS = "Ianuarie|Februarie|Martie|Aprilie|Mai|Iunie|Iulie|August|Septembrie|Octombrie|Noiembrie|Decembrie"
_MONTH_LIST = _MONTHS.split("|")
_MONTH_RE_EXPR = "({})".format(_MONTHS)
DATE_REGEX = re.compile(r"({}) (20[0-9]+)/.*".format(_MONTH_RE_EXPR))
CNP_REGEX = re.compile(r"[12][0-9]{12}")


class SheetImport:
    def __init__(self, path, version=None):
        self.path = path
        self.version = version

    def read(self):
        versions = {
            1: load_v1,
        }
        yield from versions.get(self.version, versions[max(versions)])(self.path)

    @classmethod
    def get_month(cls, payslip_data):
        for row in payslip_data:
            first_cell = row[0]
            date_match = DATE_REGEX.match(first_cell)
            if date_match and date_match.start() == 0:
                month = _MONTH_LIST.index(date_match.group(2)) + 1
                year = int(date_match.group(3))

                payday = datetime.date(year=year, month=month, day=PAYDAY)
                return payday

    @classmethod
    def get_cnp(cls, payslip_data):
        for row in payslip_data:
            second_cell = row[1]
            if isinstance(second_cell, int):
                second_cell = str(second_cell)
            if not isinstance(second_cell, str):
                continue
            cnp_match = CNP_REGEX.match(second_cell)
            if cnp_match and cnp_match.start() == 0:
                cnp = cnp_match.group()
                return cnp

    def readbigsheet(self):
        data = []
        this_user = None

        for row in self.read():
            for cell in row:
                if isinstance(cell.value, str) and cell.value.startswith("---"):
                    if this_user:
                        payday = self.get_month(this_user.data)
                        cnp = self.get_cnp(this_user.data)
                        data.append(ProfileData(
                            this_user.name, this_user.pid, this_user.function,
                            this_user.data, payday, cnp))
                    this_user = None
                    break

                if this_user is None and cell.value:
                    crnr, userstring = cell.value.split(".")
                    name, pid, function = userstring.split("/")
                    this_user = ProfileData(name, pid, function, [], None, None)
                    break

            if this_user:
                this_user.data.append(tuple(cell.value for cell in row))

        return data


def load_v1(filepath):
    wb = xlsx.load_workbook(filename=filepath, read_only=True)
    worksheet = wb[wb.sheetnames[0]]
    yield from worksheet.rows

