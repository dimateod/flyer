import os
import base64
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate

from oauth2client import file, client, tools
from httplib2 import Http
from googleapiclient.discovery import build
from apiclient import errors

from flyer.lib.config import Config


class GMail:
    SCOPES = "https://www.googleapis.com/auth/gmail.send"

    def __init__(self):
        config_dir = Config.instance.dir
        self._token_path = os.path.join(config_dir, "token.json")
        self._client_id_path = os.path.join(config_dir, "client_id.json")
        self.initialize_service()

    def initialize_service(self):
        store = file.Storage(self._token_path)
        creds = store.get()
        if not creds or creds.invalid:
            flow = client.flow_from_clientsecrets(self._client_id_path, self.SCOPES)
            args = tools.argparser.parse_args([])
            creds = tools.run_flow(flow, store, args)
        self.service = build("gmail", "v1", http=creds.authorize(Http()))
        return self.service

    def send_email(self, recipient, subject, body):
        payload = {"raw": base64.urlsafe_b64encode(body).decode("utf-8")}
        try:
            message = (self.service
                       .users()
                       .messages()
                       .send(userId="me", body=payload)
                       .execute())
            return message
        except errors.HttpError as error:
            print("An error occurred:", error, file=sys.stderr)

    @staticmethod
    def build_body(recipient, attachment_filepath, subject):
        msg = MIMEMultipart()
        msg["From"] = "me"
        msg["To"] = COMMASPACE.join([recipient])
        msg["Date"] = formatdate(localtime=True)
        msg["Subject"] = subject

        msg.attach(MIMEText("Attached you can find this month's payslip. Have a nice day!"))

        attachment_name = os.path.basename(attachment_filepath)
        with open(attachment_filepath, "rb") as file_:
            part = MIMEApplication(
                file_.read(),
                Name=attachment_name
            )
        part["Content-Disposition"] = 'attachment; filename="%s"' % attachment_name
        msg.attach(part)

        return msg.as_string().encode("utf-8")
