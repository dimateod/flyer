from contextlib import contextmanager
import datetime
import sqlite3 as _sqlite3
import json

from flyer.lib.config import Config


class DB:
    """DB backend support. This is the interface with all persistent data."""

    instance = None

    def __init__(self, uri):
        self.uri = uri
        self._connection = None

    @classmethod
    def initialize(cls, force=False):
        if cls.instance and not force:
            return cls.instance
        uri = Config.instance["db"]["sqlite"]
        cls.instance = cls(uri)
        return cls.instance

    @property
    def query(self):
        return Query(self)

    @contextmanager
    def connect(self):
        if not self._connection:
            self._connection = _sqlite3.connect(self.uri)
        yield self._connection.cursor()
        self._connection.commit()
        self._connection.close()
        self._connection = None

    def create_db(self):
        with self.connect() as cursor:
            cursor.execute("""
            CREATE TABLE profiles
            (
            pid INTEGER PRIMARY KEY,
            email TEXT NOT NULL,
            name TEXT NOT NULL,
            function TEXT NOT NULL,
            password TEXT,
            options TEXT,
            created_at TEXT NOT NULL
            )
            """)

            cursor.execute("""
            CREATE TABLE payslips
            (
            pid INTEGER NOT NULL,
            payday TEXT NOT NULL,
            data TEXT NOT NULL,
            created_at TEXT NOT NULL,
            PRIMARY KEY (pid, payday)
            )
            """)


class Query:
    def __init__(self, db):
        self.db = db

    def insert_profiles(self, profiles):
        with self.db.connect() as cursor:
            for profile in profiles:
                try:
                    cursor.execute("""INSERT INTO profiles VALUES (?,?,?,?,?,?,?)""", profile)
                except _sqlite3.IntegrityError:
                    print("Profile", profile.pid, profile.name, profile.email, "already exists")

    def upsert_profile(self, profile):
        with self.db.connect() as cursor:
            cursor.execute("""INSERT OR REPLACE INTO profiles VALUES (?,?,?,?,?,?,?)""", profile)

    def remove_profile(self, pid):
        with self.db.connect() as cursor:
            return cursor.execute("""DELETE FROM profiles WHERE pid=?""", (pid,))

    def list_profiles(self):
        with self.db.connect() as cursor:
            yield from cursor.execute("""SELECT * FROM profiles""")

    def get_profile(self, pid):
        with self.db.connect() as cursor:
            result = cursor.execute("""SELECT * FROM profiles WHERE pid=? LIMIT 1""", (pid,))
            rows = list(result)
        return rows.pop() if rows else None

    def insert_payslip(self, profile_data):
        created_at = datetime.datetime.utcnow()
        data = (profile_data.pid, profile_data.payday, profile_data.data, created_at)
        with self.db.connect() as cursor:
            cursor.execute("""INSERT OR REPLACE INTO payslips VALUES (?,?,?,?)""", data)

    def insert_payslips(self, iterable_profile_data):
        created_at = datetime.datetime.utcnow()
        with self.db.connect() as cursor:
            data_list = [(profile_data.pid, profile_data.payday,
                          json.dumps(profile_data.data), created_at)
                         for profile_data in iterable_profile_data]
            cursor.executemany("""INSERT OR REPLACE INTO payslips VALUES (?,?,?,?)""", data_list)

    def get_payslips_by_payday(self, payday):
        with self.db.connect() as cursor:
            for data in cursor.execute("""SELECT * FROM payslips WHERE payday = ?""", (payday,)):
                yield data[:2] + (json.loads(data[2]), data[3])

    def get_payslips_by_profile_payday(self, pid, payday):
        with self.db.connect() as cursor:
            for data in cursor.execute("""SELECT * FROM payslips WHERE pid = ? AND payday = ?""", (pid, payday)):
                yield data[:2] + (json.loads(data[2]), data[3])

