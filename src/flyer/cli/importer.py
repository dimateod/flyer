import click

from flyer.api.importer import Importer
from flyer.api.profiles import Profiles

from .base import main, display_profile


@main.command("import")
@click.argument("spreadsheet", type=click.Path(file_okay=True, dir_okay=False, exists=True))
@click.option("--overwrite-passwords", is_flag=True, default=False)
def importer(spreadsheet, overwrite_passwords=False):
    """Import spreadsheet data into the database"""

    importer = Importer(spreadsheet)
    data = importer.check()

    known = data["known"]
    unknown = data["unknown"]
    unknown_profiles = {p.pid: p for p in unknown}

    if unknown:
        print("Data read contains {} profiles with unknown addresses.".format(len(unknown_profiles)))
        print("Please input profile details by running a check first!")
        return

    print("Spreadsheet contains {} payslips with profiles that already exist in the database.".format(len(known)))
    for pdata in known:
        print("{} - Employee ID {} - has a payslip for month {} with {} rows"
              .format(pdata.name, pdata.pid, pdata.payday.strftime("%Y-%m"), len(pdata.data)))

    if click.confirm("Insert data into database?"):
        importer.insert(known)

    if overwrite_passwords:
        print("OVERWRITING PASSWORDS")
        for profile_data in known:
            profile = Profiles.get(profile_data.pid)
            print("-" * 40)
            print("Database Profile:")
            display_profile(profile)
            print("Imported Profile:")
            display_profile(profile_data)
            if not click.confirm("OVERWRITE FUNCTION AND PASSWORD DATA FOR {} - {}?"
                                 .format(profile.pid, profile.name)):
                print("Skip overwriting", profile.pid)
                continue
            Profiles.import_data({
                "pid": profile.pid,
                "email": profile.email,
                "name": profile_data.name,
                "function": profile_data.function,
                "password": profile_data.cnp,
                "options": profile.options,
            })

