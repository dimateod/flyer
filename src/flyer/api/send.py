import datetime

from flyer.lib.send import GMail
from flyer.lib.db import DB


SUBJECT = "Payslip {month}"


class Send:
    def __init__(self):
        self.gmail = GMail()

    def send(self, profile, payslip, file_path):
        try:
            recipient = profile.email
            payday = datetime.datetime.strptime(payslip.payday, "%Y-%m-%d")
            subject = SUBJECT.format(month=payday.strftime("%B %Y"))
            body = self.gmail.build_body(recipient, file_path, subject)
            return self.gmail.send_email(recipient, subject, body)

        except Exception as ex:
            print("Error sending emails:", ex)
