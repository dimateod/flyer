from collections import namedtuple

ProfileData = namedtuple("ProfileData", "name pid function data payday cnp")
Profile = namedtuple("Profile", "pid email name function password options created_at")

