import sys
import pkg_resources
import datetime
import pprint

import click
import yaml

from flyer.lib.config import Config
from flyer.lib.db import DB
from flyer.lib.models import Profile

from .base import main


NAME = "flyer"


@main.group()
def meta():
    pass


@meta.command()
def info():
    """Print info about the package"""
    print("version:", pkg_resources.get_distribution(NAME).version)
    print("python:", '.'.join(str(c) for c in sys.version_info))
    print("binary:", sys.executable)
    config = Config.instance
    print("config:", config.path)
    print("db:", config["db"]["sqlite"])


@meta.command()
def create_db():
    DB.initialize().create_db()


@meta.command()
@click.argument("config", type=click.File("r"))
def migrate(config):
    data = yaml.safe_load(config)

    profiles = []
    for id_, email in data["addresses"].items():
        name, pid = id_.split(" - ")
        function = "N/A"
        profiles.append(Profile(int(pid), email, name, function, None, None, datetime.datetime.utcnow()))

    DB.initialize().query.insert_profiles(profiles)

    print(yaml.dump(profiles, default_flow_style=False))
