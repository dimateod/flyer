import subprocess
import os
import tempfile

import openpyxl as xlsx


class _Exporter:

    def create_temp_file(self, *args, **kwargs):
        raise NotImplementedError

    def export(self, *args, **kwargs):
        raise NotImplementedError

    def get_temp_path(self, name=None):
        name = name or self.default_name
        tempdir = tempfile.gettempdir()
        file_name = "{base}.{ext}".format(base=name, ext=self.EXT.strip("."))
        filepath = os.path.join(tempdir, file_name)
        return filepath

    def clear(self, path):
        try:
            os.remove(path)
        except FileNotFoundError:
            pass
        return path


class Archive(_Exporter):

    EXT = "zip"

    def __init__(self, another_exporter, protect_password=None):
        self._exporter = another_exporter
        self.protect_password = protect_password
        self.default_name = self._exporter.default_name

    def create_temp_file(self, name=None):
        archivepath = self.clear(self.get_temp_path(name))
        filepath = self._exporter.create_temp_file()
        self.pack(filepath, archivepath)
        return archivepath

    def pack(self, filepath, archivepath):
        cmd = self._cmd(archivepath, filepath, self.protect_password)
        print(">>>", cmd)
        sp = subprocess.run(cmd, check=True, stdout=subprocess.DEVNULL)

    @staticmethod
    def _cmd(archivepath, filepath, password=None):
        args = ["7z", "a", "-y"]
        if password:
            args += ["-mem=AES256", "-p{}".format(password)]
        args += [archivepath, filepath]
        return args


class Spreadsheet(_Exporter):

    EXT = "xlsx"

    def __init__(self, payslip):
        self.data = payslip.data
        self.default_name = "{}_{}".format(payslip.payday, payslip.pid)

    def create_temp_file(self, name=None):
        filepath = self.clear(self.get_temp_path(name))
        self.export(filepath)
        return filepath

    def export(self, path):
        wb = xlsx.Workbook()
        current = wb.active
        for row in self.data:
            current.append(row)
        wb.save(path)
        return path

