import click

from flyer.api.send import Send
from flyer.api.exporter import Exporter
from flyer.api.profiles import Profiles
from .base import main


@main.command()
@click.option("-p", "--profile-id", type=int, help="Send email only to this profile")
@click.option("-m", "--month", type=click.DateTime(formats=("%Y-%m",)),
              help="Month to send")
def send(profile_id=None, month=None):
    """Send emails with payslips for the current month"""
    if month:
        print("Searching for payslips in month", month)
    else:
        print("Searching for payslips in current month")

    sender = Send()
    payslips = Exporter.get_monthly_payslips(month, profile_id)
    if payslips:
        print("Found {} payslips for payday {}".format(len(payslips), payslips[0].payday))
    else:
        print("No payslips found!")
    for payslip in payslips:
        if profile_id and payslip.pid != profile_id:
            continue
        profile = Profiles.get(payslip.pid)
        if not click.confirm("Send payslip email for {} - ID {} - to email address {}?"
                             .format(profile.name, profile.pid, profile.email)):
            print("OK, not sent")
            continue
        print("Sending email to {}".format(profile.email))
        with Exporter.temporary_export(payslip, profile.password) as temp_path:
            try:
                sender.send(profile, payslip, temp_path)
            except Exception as ex:
                print("Error occurred:", ex)

