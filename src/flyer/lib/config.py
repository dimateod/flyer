import logging
import logging.config
import os

import yaml

from .constants import (
    DEFAULT_CONFIG_LOCATION,
    DEFAULT_CONFIG_PATH,
    DEFAULT_CONFIG_PATHS,
    PACKAGE_NAME,
)


def _get_some_config_path():
    """Get a valid config path from a list of defaults.

    Returns None if no existing file was found at one of the paths.

    """
    for l in DEFAULT_CONFIG_PATHS:
        try:
            with open(l):
                return l
        except IOError:
            pass
    return None


class Config(dict):

    TEMPLATE = {
        "db": {"sqlite": os.path.join(DEFAULT_CONFIG_LOCATION, "flyer.1.db")},
        "logging": {
            "version": 1,
            "formatters": {
                "simple": {
                    "class": "logging.Formatter",
                    "format": "%(asctime)s | %{levelname}s | %{message}s",
                    "datefmt": "%Y-%m-%dT%H:%M:%S",
                }
            },
            "handlers": {
                "console": {
                    "level": "DEBUG",
                    "formatter": "simple",
                    "class": "logging.StreamHandler",
                },
            },
            "loggers": {
                PACKAGE_NAME: {
                    "handlers": ["console"],
                    "level": "DEBUG",
                },
            },
            "root": {
                "handlers": [],
                "level": "ERROR",
            }
        },
    }

    instance = None

    def __init__(self, path=None):
        super().__init__(self.TEMPLATE)
        path = path or _get_some_config_path()
        if path is None:
            self.init_default()
        else:
            self.path = path
            self.dir = os.path.dirname(self.path)
        # self.log = self.configure_logging()

    @classmethod
    def initialize(cls, path=None):
        if cls.instance:
            return cls.instance
        cls.instance = cls(path)
        return cls.instance

    @classmethod
    def _recursive_update(cls, config, data):
        for k, v in data.items():
            if isinstance(v, dict) and isinstance(config.get(k, {}), dict):
                cls._recursive_update(config.setdefault(k, {}), v)
            else:
                config[k] = v

    def __getitem__(self, key):
        if not self:
            self.load()
        return super().__getitem__(key)

    def __setitem__(self, key, value):
        result = super().__setitem__(key, value)
        self.save()
        return result

    def init_default(self):
        self.dir = DEFAULT_CONFIG_LOCATION
        self.path = DEFAULT_CONFIG_PATH
        if not os.path.exists(self.dir):
            os.mkdir(self.dir)

    def load(self):
        with open(self.path) as conffile:
            data = yaml.safe_load(conffile)
        self._recursive_update(self, data)

    def save(self):
        with open(self.path, "w") as conffile:
            yaml.safe_dump(self, conffile, default_flow_style=False)

    def configure_logging(self):
        logging.config.dictConfig(self["logging"])
        logger = logging.getLogger(PACKAGE_NAME)
        logger.info("Logging configured")
