import datetime

from flyer.lib.config import Config
from flyer.lib.models import Profile, ProfileData
from flyer.lib.db import DB


class Profiles:
    @staticmethod
    def list():
        for row in DB.instance.query.list_profiles():
            yield Profile(*row)

    @classmethod
    def import_data(cls, data):
        if "created_at" not in data:
            data["created_at"] = datetime.datetime.utcnow()
        return cls.save(Profile(**data))

    @classmethod
    def save(cls, profile):
        return DB.instance.query.upsert_profile(profile)

    @staticmethod
    def get(pid):
        data = DB.instance.query.get_profile(pid)
        if data:
            return Profile(*data)
        else:
            return None

    @staticmethod
    def remove(pid):
        return DB.instance.query.remove_profile(pid)
