from setuptools import setup

APP = ["flyer"]
DATA_FILES = []
OPTIONS = {"argv_emulation": True,
           "iconfile": "res/email.png.icns"}

setup(
    app=APP,
    data_files=DATA_FILES,
    options={"py2app": OPTIONS},
    package_dir={"": "src"},
    package_data={"": ["*.json"]},
    entry_points={
        "console_scripts": [
            "flyer = flyer.cli:main",
        ]
    },
)
