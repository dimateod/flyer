import os

import appdirs


PACKAGE_NAME = __package__.split(".", 1)[0]

DEFAULT_CONFIG_LOCATION = appdirs.user_config_dir(PACKAGE_NAME, "")
DEFAULT_CONFIG_LOCATIONS = (
    DEFAULT_CONFIG_LOCATION,
    ".",
)
DEFAULT_CONFIG_PATHS = [os.path.join(cdir, "config.yaml") for cdir in DEFAULT_CONFIG_LOCATIONS]
DEFAULT_CONFIG_PATH = os.path.join(DEFAULT_CONFIG_LOCATION, "config.yaml")

PAYDAY = 3
