from flyer.api.profiles import Profiles
from flyer.lib.importer import spreadsheet
from flyer.lib.db import DB


class Importer:
    def __init__(self, path):
        self.path = path
        self._data = None
        self._lib = spreadsheet.SheetImport(path)

    @property
    def data(self):
        if not self._data:
            self._data = self._lib.readbigsheet()
        return self._data

    def check(self):
        DB.initialize()
        profiles = {p.pid: p for p in Profiles.list()}
        known_profiles = []
        unknown_profiles = []
        for prof_data in self.data:
            pid = int(prof_data.pid)
            if pid in profiles:
                known_profiles.append(prof_data)
            else:
                unknown_profiles.append(prof_data)
        return {"known": known_profiles,
                "unknown": unknown_profiles}

    @classmethod
    def insert(cls, profile_data_list):
        DB.initialize().query.insert_payslips(profile_data_list)

