import os
from contextlib import contextmanager
import datetime

from flyer.lib.db import DB
from flyer.lib.models import Payslip
from flyer.lib.constants import PAYDAY
from flyer.lib.exporter import Spreadsheet, Archive


class Exporter:

    @contextmanager
    def temporary_export(payslip, password=None):
        spreadsheet_exporter = Spreadsheet(payslip)
        archive_exporter = Archive(spreadsheet_exporter, password)
        temp_path = archive_exporter.create_temp_file()
        yield temp_path
        archive_exporter.clear(archive_exporter.get_temp_path())
        spreadsheet_exporter.clear(spreadsheet_exporter.get_temp_path())

    @classmethod
    def get_monthly_payslips(cls, month_date=None, profile_id=None):
        if month_date:
            payday = cls.get_month_payday(month_date)
        else:
            payday = cls.get_current_month_payday()
        if profile_id:
            payslips = (DB.initialize().query.
                        get_payslips_by_profile_payday(profile_id, payday))
        else:
            payslips = DB.initialize().query.get_payslips_by_payday(payday)
        return list(Payslip(*p) for p in payslips)

    @classmethod
    def get_current_month_payday(cls):
        now = datetime.date.today()
        return cls.get_month_payday(now)

    @classmethod
    def get_month_payday(cls, month_date):
        return datetime.date(month_date.year, month_date.month, PAYDAY)

