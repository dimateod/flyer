import click

from flyer.api.profiles import Profiles
from flyer.lib.db import DB
from flyer.lib.models import Profile

from .base import main, display_profile


@main.group()
def profiles():
    DB.initialize()


@profiles.command()
@click.option("--pid", type=int, default=None)
def show(pid=None):
    if pid:
        display_profile(Profiles.get(pid))
    else:
        for profile in Profiles.list():
            display_profile(profile)


@profiles.command()
@click.argument("profile_id", type=int)
def set(profile_id):
    """Set profile data"""
    profile = Profiles.get(profile_id)
    if profile:
        print("Profile already exists:")
        display_profile(profile)
        print("Modify existing data:")
    else:
        print("New profile:")

    data = {"pid": profile_id}
    for k in Profile._fields:
        if profile:
            default = getattr(profile, k)
            value = input("{} (default: {}): ".format(k, default)).strip() or default
        else:
            value = input("{}: ".format(k))
        data[k] = value

    new_profile = Profile(**data)
    Profiles.save(new_profile)

    print("Inserted profile:")
    display_profile(new_profile)


@profiles.command()
@click.argument("profile_id", type=int)
def remove(pid):
    Profiles.remove(pid)

