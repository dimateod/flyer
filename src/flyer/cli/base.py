import click

from flyer.lib.config import Config


@click.group()
@click.option("-c", "--config", required=False,
              type=click.Path(dir_okay=False, writable=True, readable=True))
def main(config=None):
    Config.initialize(config)


def display_profile(profile, invisible=("name", "data")):
    print(profile.name)
    for key, value in profile._asdict().items():
        if key in invisible: continue
        print("  {}: {}".format(key, value))
