import click

from flyer.api.profiles import Profiles
from flyer.api.importer import Importer

from .base import main, display_profile


@main.command()
@click.argument("spreadsheet-path",
                type=click.Path(exists=True, file_okay=True, dir_okay=False, readable=True))
def check(spreadsheet_path):
    """Check spreadsheet at path for validity"""

    importer = Importer(spreadsheet_path)
    data = importer.check()

    known = data["known"]
    unknown = data["unknown"]
    unknown_profiles = {p.pid: p for p in unknown}

    if unknown:
        print("Data read contains {} profiles with unknown addresses.".format(len(unknown_profiles)))
        print("Please input profile details:")
    for profile in unknown_profiles.values():
        display_profile(profile)
        address = input("Email address: ")
        Profiles.import_data({
            "pid": profile.pid,
            "email": address,
            "name": profile.name,
            "function": profile.function,
            "password": profile.cnp,
            "options": None,
        })
    print("Spreadsheet contains {} payslips with profiles that already exist in the database.".format(len(known)))
    for pdata in known:
        print("{} - Employee ID {}"
              .format(pdata.name, pdata.pid))
        print(" - has a payslip for month {} with {} rows"
              .format(pdata.payday.strftime("%Y-%m"), len(pdata.data)))
